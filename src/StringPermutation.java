/**
 * Created by folawiyocampbell on 1/31/17.
 */
public class StringPermutation {

    public static void main(String[] args) {
        String orig = "akaarea";
        String perm = "reaaak";

        System.out.println(isAPermutation(orig,perm));
    }


    public static boolean isAPermutation(String original, String perm){
        char [] asciiCh = new char [128] ;
        char [] oCharz = original.toCharArray();// a k a a
        char [] permCharz = perm.toCharArray();// a k

        if(oCharz.length!=permCharz.length){
            return false;
        }

        for(char ch: oCharz){
            asciiCh[ch]++;
        }


        //a ->3
        //k ->1

        for(char ch: permCharz){
            asciiCh[ch]--;
            if(asciiCh[ch]<0){
                return false;
            }
        }




        return true;


    }
}
