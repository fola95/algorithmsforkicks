import java.util.List;

/**
 * Created by folawiyocampbell on 1/31/17.
 */
public class RotateAMatrixBy90Degrees {

    public static void main(String[] args) {

        int [][] matrix ={{1,2,3},{1,2,3},{1,2,3}};



        rotateMatrixBy90(matrix);

        for(int i=0; i<matrix.length;i++){
            System.out.println(matrix[2][i]);
        }
    }

    public static int[][] rotateMatrixBy90(int[][]matrix){

        int layer = matrix.length/2;

        for(int i=0; i<layer; i++){
            int first=i;
            int last =matrix.length-1-i;
            for(int k=first;  k<last;k++ ){
                int offset = k-first;

                int top =matrix[first][k];
                //t<-l
                matrix[first][k]= matrix[last-offset][first];
                //l<-b
                matrix[last-offset][first] = matrix[last][last-offset];
                //b<-r
                matrix[last][last-offset] =matrix[k][last];

                matrix[k][last]=top;

            }


        }

        return matrix;
    }
}
