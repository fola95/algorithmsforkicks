/**
 * Created by folawiyocampbell on 1/31/17.
 */
public class UniqueCharacters {


    public static void main(String[] args) {
        String s = "abcde";
        String sN = "ababaed";

        System.out.println(isUnique(sN));
        System.out.println(isUnique(s));
    }



    public static  boolean isUnique(String s){
        int [] charz = new int [128];

        char [] sChars = s.toCharArray();

        for(char ch : sChars){
            if(charz[ch]==1){
                return false;
            }

            charz[ch]=1;

        }

        return true;





    }
}
