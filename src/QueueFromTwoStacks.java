import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Created by folawiyocampbell on 8/29/17.
 */
public class QueueFromTwoStacks {

    private Stack<Integer> pri = new Stack<>();
    private Stack<Integer> sec = new Stack<>();

    public  void rebalance (){
        while(!sec.isEmpty()){
            Integer z =sec.pop();
            pri.push(z);
        }
    }

    public void enqueue(Integer num){
        if(pri.isEmpty()){
            rebalance();
        }
        sec.add(num);
    }

    public Integer dequeue(){
        if(pri.isEmpty()){
            rebalance();
        }

        if(pri.isEmpty()){
            throw new NoSuchElementException("cannot dequeue from an empty queue");
        }
        return pri.pop();
    }

    public static void main(String[] args) {
        //quick test of this data structure

        QueueFromTwoStacks q = new QueueFromTwoStacks();
        q.enqueue(1);
        q.enqueue(4);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        q.enqueue(3);
        System.out.println(q.dequeue());


    }

}
