/**
 * Created by folawiyocampbell on 2/5/17.
 */
public class CakeTheifProblem {

    public static void main(String[] args) {
        CakeType[] cakes ={new CakeType(7,160), new CakeType(3,90), new CakeType(2,15)};

       int ans = maxVal(cakes,20);
        System.out.println(ans);
    }

    public static int maxVal(CakeType[] cakes, int capacity){

        int i=0;
        int [] maxVals = new int [capacity+1];
        int comp[] = new  int[cakes.length];

        while(i<=capacity){

            for(int k=0; k<cakes.length; k++){

                if(i>=cakes[k].weight){
                    comp[k]=maxVals[i-cakes[k].weight]+cakes[k].value;
                }else{
                    comp[k]=0;
                }
            }

            for(int val: comp){
                maxVals[i]=val>maxVals[i]?val:maxVals[i];
            }
            i++;
        }

        return maxVals[capacity];

    }

    static class CakeType{
        int weight=0;
        int value=0;


        public CakeType(int weight, int value){
            this.weight=weight;
            this.value=value;
        }
    }

}
