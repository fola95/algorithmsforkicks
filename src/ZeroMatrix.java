import java.util.HashSet;
import java.util.Set;

/**
 * Created by folawiyocampbell on 1/31/17.
 */
public class ZeroMatrix {

    //matirx[r][c]
    public static void main(String[] args) {

    }

    public static int[][] blankOutMatrix(int [][] matrix){
        int rLen= matrix.length;
        int cLen= matrix[0].length;
        Set <Integer> rToClear = new HashSet<>();
        Set <Integer> cToClear = new HashSet<>();

        for(int r=0; r<rLen; r++ ){

            for(int c =0; c<cLen; c++){
                if(matrix[r][c]==0){
                    rToClear.add(r);
                    cToClear.add(c);
                }

            }
        }

        for(Integer in: rToClear){
            matrix=blankOutRow(matrix,in,cLen);
        }

        for(Integer in: cToClear){
            matrix = blankOutColumn(matrix,in,rLen);
        }

        return matrix;
    }

    public static int[][] blankOutRow(int [][] matrix,int r, int cLen){
        for(int i=0; i<cLen; i++){
            matrix[r][i]=0;
        }
        return matrix;
    }

    public  static int[][] blankOutColumn(int [][] matrix, int c, int rLen){
        for(int i=0; i<rLen; i++){
            matrix[i][c]=0;
        }
        return matrix;
    }
}
