import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by folawiyocampbell on 2/6/17.
 */
public class TopologicalSort {

    private static Stack<Node>finalAns = new Stack<>();

    public static void main(String[] args) {

        Node underG= new Node("undershorts");
        Node pants = new Node("pants");
        Node belt = new Node("belt");
        Node shirt = new Node("shirt");
        Node tie = new Node("tie");
        Node jacket = new Node ("jacket");
        Node socks = new Node("socks");
        Node shoes = new Node("shoes");
        Node watch = new Node("watch");

        shirt.neighs.add(belt);
        shirt.neighs.add(tie);
        tie.neighs.add(jacket);
        socks.neighs.add(shoes);


        underG.neighs.add(pants);
        underG.neighs.add(shoes);
        pants.neighs.add(belt);
        pants.neighs.add(shoes);
        belt.neighs.add(jacket);

        topologicalSort( Arrays.asList(new Node[]{underG,pants,belt,shirt,tie,jacket,socks,shoes,watch}));

        while(!finalAns.isEmpty()){
            Node n = finalAns.pop();
            System.out.println(n.garmet);
        }


    }

    static class Node{
        String garmet ="";
        boolean visited =false;
        boolean visitedAllNeigh =false;
        List<Node> neighs = new ArrayList<>();
        public Node(String garment){
            this.garmet = garment;

        }
    }

    public static void topologicalSort(List<Node> graph){

        for(Node n: graph){

            if(!n.visited){
                n.visited=true;

                if(n.neighs.size()>0 && ! n.visitedAllNeigh){
                    topologicalSort(n.neighs);
                    n.visitedAllNeigh = true;
                }
                    finalAns.push(n);


            }
        }

    }
}
