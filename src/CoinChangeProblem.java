/**
 * Created by folawiyocampbell on 2/2/17.
 */
public class CoinChangeProblem {

    public static void main(String[] args) {
        int k=numOfWays(25,0);
        System.out.println(k);
    }

    public static int numOfWays(int n, int curr){
        int n5=0,n10=0,n25=0;
        if(curr==n){
            return 1;
        }

        if((curr+5)<=n){
            n5=numOfWays(n,curr+5);

        }

        if((curr+10)<=n){
            n10=numOfWays(n,curr+10);

        }

        if((curr+25)<=n){
            n25=numOfWays(n,curr+25);

        }

        return n5+n10+n25;
    }
}
