import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by folawiyocampbell on 2/5/17.
 */
public class ListOfDepths {


    public static List<LinkedList<Node>> makeDepthList(Node root){
        Queue<LinkedList<Node>> q = new LinkedList<>();
        List<LinkedList<Node>> finalList = new ArrayList<>();
        LinkedList<Node> l1 = new LinkedList<>();

        l1.add(root);
        q.add(l1);
        finalList.add(l1);

        while(!q.isEmpty()){
            LinkedList<Node> l = q.poll();
            LinkedList<Node> nLev = new LinkedList<>();
            for(Node k: l){

                if(k.left!=null)
                    nLev.add(k.left);

                if(k.right!=null)
                    nLev.add(k.right);
            }

            if(!nLev.isEmpty())
                q.add(nLev);

            finalList.add(nLev);

        }

        return finalList;

    }

    static class Node{
        int val;
        Node left;
        Node right;

        Node(int val){

        }
    }
}
