import java.util.HashMap;
import java.util.Map;

/**
 * Created by folawiyocampbell on 1/31/17.
 */
public class PermutationOfPalindrome {


    public static void main(String[] args) {
        String s="caabbkkbtbaac";
        System.out.println(isPermOfPal(s));
    }



    public static boolean isPermOfPal(String s){
        char [] chz = s.toCharArray();
        Map<Character, Integer> map = new HashMap<>();


        //count the occurence of each character
        for(char ch: chz){
            if(map.get(ch)!=null){
                int k = map.get(ch);
                k++;
                map.put(ch,k);
            }else{
                map.put(ch,1);
            }
        }

        //if more than one odd number of character
        int oddCount = 0;
        int len = s.length();

        for(Map.Entry<Character,Integer> entry: map.entrySet()){

            if(len%2==1){

                if(entry.getValue()%2==1){
                    oddCount++;
                }

                if(oddCount>1){
                    return false;
                }
            }else{
                if(entry.getValue()%2==1){
                    return false;
                }
            }

        }

        return true;

    }
}
